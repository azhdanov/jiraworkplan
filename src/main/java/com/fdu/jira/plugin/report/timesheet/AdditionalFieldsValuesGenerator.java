package com.fdu.jira.plugin.report.timesheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.SearchableField;
import com.osf.jira.plugins.util.TextUtil;

public class AdditionalFieldsValuesGenerator implements ValuesGenerator {
    private final FieldManager fieldManager;

    public AdditionalFieldsValuesGenerator() {
        fieldManager = ComponentAccessor.getFieldManager();
    }

    public Map<String, String> getValues(Map params) {
        Map<String, String> values = new LinkedHashMap<String, String>();
        values.put("", "None");

        
        Set<SearchableField> fields = fieldManager.getAllSearchableFields();

        List<Field> sortedFields = new ArrayList<Field>(fields.size());
        sortedFields.addAll(fields);
        sortedFields.add(fieldManager.getField("timeoriginalestimate"));
        sortedFields.add(fieldManager.getField("timeestimate"));
        sortedFields.add(fieldManager.getField("timespent"));

        Collections.sort(sortedFields, new Comparator<Field>() {
            public int compare(Field o, Field other) {
                return o.getName().compareTo(other.getName());
            }
        });

        for (Field field : sortedFields) {
            values.put(field.getId(), TextUtil.getUnquotedString(field.getName()));
        }

        return values;
    }
}
