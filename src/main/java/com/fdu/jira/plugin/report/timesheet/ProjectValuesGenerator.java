package com.fdu.jira.plugin.report.timesheet;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.user.ApplicationUser;
import com.osf.jira.plugins.util.TextUtil;

import webwork.action.ActionContext;

public class ProjectValuesGenerator implements ValuesGenerator {

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map<String, String> getValues(Map userParams) {
        ProjectManager projectManager = 
            ComponentAccessor.getProjectManager();
        Collection<Project> projects = projectManager.getProjectObjects();
        PermissionManager permissionManager = 
            ComponentAccessor.getPermissionManager();
        JiraAuthenticationContext authenticationContext =
            ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser remoteUser = authenticationContext.getLoggedInUser();
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put("", "All Projects");
        for (Project project : projects) {
            if (permissionManager.hasPermission(Permissions.BROWSE, project,
                    remoteUser)) {
                result.put(project.getId().toString(),  TextUtil.getUnquotedString(project.getName()));
                
            }
        }
        if (ActionContext.getRequest().getParameter("projectid") == null
                && ActionContext.getRequest().getParameter("selectedProjectId") != null) {
            Map actionParameters = new HashMap(ActionContext.getParameters());
            actionParameters.put("projectid",
                    new String[] {ActionContext.getRequest().getParameter("selectedProjectId")});
            ActionContext.setParameters(actionParameters);
        }
        return result;
    }
}

