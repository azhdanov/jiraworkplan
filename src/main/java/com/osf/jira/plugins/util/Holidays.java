/**
 * Copyright (c) 2010, Andriy Zhdanov,
 * azhdanov@gmail.com
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the Outsourcing Factory Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package com.osf.jira.plugins.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;

/**
 * Load holidays from jira-application.properties,
 * where they must be specified basing on relevant government site manually, e.g.
 * E.g. WEB-INF/classes/jira-application.properties:
 * <code>
 * jira.workplan.report.plugin.holidays=\
 * 18/May/2010 - Free Day,\
 * 24/May/2010 - Another Free Day
 * </code>
 * See also (for future) http://www.javaworld.com/javaworld/javatips/jw-javatip44.html
 */
public class Holidays {
    private static final Logger log = Logger.getLogger(Holidays.class);

    private static Map holidays;
    public static Map getHolidays() {
        if (holidays == null) {
            synchronized (Holidays.class) {
                if (holidays == null) {
                    holidays = new TreeMap();
                    ApplicationProperties ap = ComponentAccessor.getApplicationProperties();
                    String p = ap.getDefaultBackedString("jira.workplan.report.plugin.holidays");
                    log.debug("There is no 'jira.workplan.report.plugin.holidays' application property set");
                    if (p == null) {
                        return holidays;
                    }

                    Locale locale = Locale.getDefault();
                    String localeStr = ap.getDefaultBackedString("jira.i18n.default.locale");
                    if (localeStr != null) {
                        locale = new Locale(localeStr);
                    }
                    log.debug("Using " + locale.getDisplayName() + " locale for parsing holidays");
                    
                    String formatStr = ap.getDefaultBackedString("jira.lf.date.dmy");
                    log.debug("Using " + formatStr + " holidays date format");
                    DateFormat df = new SimpleDateFormat(formatStr, locale);
                    

                    String[] s = p.split(",");
                    for (int i = 0; i < s.length; i++) {
                        String[] t = s[i].split(" - ");
                        try {
                            Date date = df.parse(t[0]);
                            holidays.put(date, t[1]);
                        } catch (ParseException e) {
                            log.debug("Can't parse holiday date: " + t[0], e);
                            continue;
                        }
                    }
                }
            }
        }
        return holidays;
    }

}
