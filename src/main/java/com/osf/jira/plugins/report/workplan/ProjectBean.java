package com.osf.jira.plugins.report.workplan;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectBean {
    private String key;
    private String name;
    private List estimatedWorks = new ArrayList();
    private Map estimateTotals = new HashMap();
    private Map<Long, Long> dueDateTotals = new HashMap<Long, Long>();
 
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	
    public List getEstimatedWorks() {
        return estimatedWorks;
    }

    public Map getEstimateTotals() {
        return estimateTotals;
    }

    public Map<Long, Long> getDueDateTotals() {
        return dueDateTotals;
    }
}
