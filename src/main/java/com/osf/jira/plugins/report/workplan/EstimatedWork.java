/**
 * Copyright (c) 2008, Outsourcing Factory Inc.,
 * http://www.outsourcing-factory.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of the Outsourcing Factory Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
package com.osf.jira.plugins.report.workplan;

import java.util.Comparator;
import java.util.Date;
import java.util.Set;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;

/**
 * @author Andriy Zhdanov
 */
public class EstimatedWork {
    private Issue issue;
    private Set splits;

    public EstimatedWork(Issue issue) {
        this.issue = issue;
    }

    public Issue getIssue() {
        return issue;
    }

    public String getKey() {
        return issue.getKey();
    }

    public String getSummary() {
        return issue.getSummary();
    }

    public ApplicationUser getAssignee() {
        return issue.getAssignee();
    }

    public Project getProject() {
        return issue.getProjectObject();
    }

    public IssueType getIssueTypeObject() {
        return issue.getIssueTypeObject();
    }

    public Priority getPriorityObject() {
        return issue.getPriorityObject();
    }

    public Resolution getResolutionObject() {
        return issue.getResolutionObject();
    }

    public Status getStatusObject() {
        return issue.getStatusObject();
    }

    public Date getDueDate() {
        return issue.getDueDate();
    }

    public Long getEstimate() {
        return issue.getEstimate();
    }

    public void setSplits(Set splits) {
        this.splits = splits;
    }

    public Set getSplits() {
        return splits;
    }

    public static class Split {
        private Date startDate;
        private long estimate;
        private String utilizationStr;
        private double utilization;
        private int estimateInDays;

        public long getStartTime() {
            return startDate.getTime();
        }

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public long getEstimate() {
            return estimate;
        }

        public void setEstimate(long estimate) {
            this.estimate = estimate;
        }

        public void setUtilizationStr(String utilizationStr) {
            this.utilizationStr = utilizationStr;
        }

        public String getUtilizationStr() {
            return utilizationStr;
        }

        public double getUtilization() {
            return utilization;
        }

        public void setUtilization(double utilization) {
            this.utilization = utilization;
        }

        public int getEstimateInDays() {
            return estimateInDays;
        }

        public void setEstimateInDays(int estimateInDays) {
            this.estimateInDays = estimateInDays;
        }
    }
	
    /**
    * Assumes comparable objects are not null.
    */
    public static class SplitComparator implements Comparator {

        public int compare(Object arg0, Object arg1) {
            Split split1 = (Split) arg0;
            Split split2 = (Split) arg1;
            return split1.getStartDate().compareTo(split2.getStartDate());
        }

    }
}
