package com.osf.jira.plugins.report.workplan.values;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.atlassian.configurable.ValuesGenerator;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.issue.status.Status;
import com.osf.jira.plugins.util.TextUtil;

public class StatusValuesGenerator implements ValuesGenerator {

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map<String, String> getValues(Map userParams) {
        StatusManager statusManager = 
            ComponentAccessor.getComponentOfType(StatusManager.class);
        Collection<Status> statuses = statusManager.getStatuses();
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put("", "Open Issues");
        for (Status status : statuses) {
            result.put(status.getId().toString(),  TextUtil.getUnquotedString(status.getName()));
        }
        return result;
    }
}