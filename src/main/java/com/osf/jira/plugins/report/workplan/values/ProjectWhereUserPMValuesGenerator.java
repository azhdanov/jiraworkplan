package com.osf.jira.plugins.report.workplan.values;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;

public class ProjectWhereUserPMValuesGenerator {
    private static final Logger log = Logger
            .getLogger(ProjectWhereUserPMValuesGenerator.class);

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Map getValues(Map params) {
        JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        ApplicationUser remoteUser = authenticationContext.getLoggedInUser();
        try {
            Map projects = new LinkedHashMap();
            projects.put("", "");
            ProjectManager projectManager = ComponentAccessor.getProjectManager(); 
            ProjectRoleManager projectRoleManager = ComponentAccessor
                    .getComponentOfType(ProjectRoleManager.class);
            Collection<Project> allProjects = projectManager.getProjectObjects();
            ProjectRole pmRole = projectRoleManager
                    .getProjectRole("project-manager");
            for (Project project: allProjects) {
                if (projectRoleManager.isUserInProjectRole(remoteUser, pmRole, project)) {
                    projects.put(project.getId().toString(), project.getName());
                }
            }
            return projects;
        } catch (Exception e) {
            log.error("Could not retrieve project values for this user: "
                            + remoteUser.getName(), e);
            return null;
        }
    }
}
